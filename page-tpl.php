<?php
        chdir($_SERVER['DOCUMENT_ROOT']);
	require './includes/bootstrap.inc';
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

    <head>
        <meta name="robots" content="noindex, nofollow" />
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />
        <title><?php print 'Alert Overlay' ?></title>
        <script src="/misc/jquery.js" type="text/javascript"></script>
        <script src="script/jquery.corner.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function () {
    
            //round corner
            $('.alerts-container').corner();

        });
        </script>
    </head>
    
    <body>
        <?php foreach($_GET as $k => $v) { ?>
            <?php if(substr($k, 0, 3) == 'nid') : ?>
                <?php $nodo = node_load($v); ?>
                <div class="alerts-container">
                    <div class="alert-title">
                        <p><strong><?php print $nodo->title; ?></strong></p>
                    </div>
                    <div class="alert-body">
                        <p><span><?php print $nodo->body; ?></span></p>
                    </div>
                </div>
                <hr />
            <?php endif; ?>
        <?php } ?>
    </body>
</html>
  
