<?php

/**
 * Implementation of hook_strongarm().
 */
function alert_pack_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_alert';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-4',
    'revision_information' => '5',
    'author' => '4',
    'options' => '6',
    'comment_settings' => '10',
    'menu' => '1',
    'book' => '3',
    'path' => '9',
    'attachments' => '8',
    'print' => '7',
    'workflow' => '2',
  );

  $export['content_extra_weights_alert'] = $strongarm;
  return $export;
}
