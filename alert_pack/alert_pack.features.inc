<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function alert_pack_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function alert_pack_node_info() {
  $items = array(
    'alert' => array(
      'name' => t('Alert'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'has_body' => '1',
      'body_label' => t('Testo'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function alert_pack_views_api() {
  return array(
    'api' => '2',
  );
}
